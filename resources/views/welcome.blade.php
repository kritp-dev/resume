<!doctype html>
<html lang="{{ app()->getLocale() }}" ng-app="autocv">
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AutoCV.dev</title>
        <link href="{!! asset('css/app.css') !!}" rel="stylesheet" type="text/css">
        <link href="{!! asset('css/animate.css') !!}" rel="stylesheet" type="text/css">
        <link href="{!! asset('css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
        <link href="{!! asset('css/style.css') !!}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid not-print">
            <div class="wrapper">
                <h2>autocv.dev</h2>
                <nav>
                    <ul>
                        <li><a href="#!/new" class="op-icon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> New</a></li>
                        <li>&nbsp;&nbsp;&nbsp;</li>
                        <li><a href="/" class="op-icon"><i class="fa fa-book" aria-hidden="true"></i> CV List</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="container" ng-view></div>
        <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/angular.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/angular-route.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('app/main.js') !!}"></script>
        @if (session('status'))
        <div class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">{{ session('status') }}</div>
                    <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>$('.modal').modal('show');</script>
        @endif

        @if ($errors->any())
        <div class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content alert alert-danger">
                    <div class="modal-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>$('.modal').modal('show');</script>
        @endif
    </body>
</html>
