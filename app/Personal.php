<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    protected $table = 'personals';
    protected $fillable = [
        'name',
        'image',
        'age',
        'telephone',
        'email',
        'address',
        'about_me',
        'skills',
        'communications',
        'interests',
        'employments',
        'educations',
        'computers',
    ];

    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';
}
