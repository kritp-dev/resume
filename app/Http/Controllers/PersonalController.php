<?php

namespace App\Http\Controllers;

use App\Personal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Personal::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'age' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'about_me' => 'required',
            'skill.*' => 'required',
            'skill_score.*' => 'required',
            'skill_full_score.*' => 'required',
            'communication.*' => 'required',
            'comm_score.*' => 'required',
            'interest.*' => 'required',
            'position.*' => 'required',
            'company.*' => 'required',
            'period_from.*' => 'required',
            'period_to.*' => 'required',
            'position_detail.*' => 'required',
            'education.*' => 'required',
            'computer.*' => 'required',
        ]);

        $rq_skills = $request->get('skill');
        $rq_skill_score = $request->get('skill_score');
        $rq_skill_full_score = $request->get('skill_full_score');
        $skills = '[';
        foreach ($rq_skills as $key => $value) {
            $skills .= (($key>0) ? ',' : '').'{';
                $skills .= '"skill":"'.$value.'"';
                $skills .= ', "skill_score":"'.$rq_skill_score[$key].'"';
                $skills .= ', "skill_full_score":"'.$rq_skill_full_score[$key].'"';
            $skills .= '}';
        }
        $skills .= ']';

        $rq_communication = $request->get('communication');
        $rq_comm_score = $request->get('comm_score');
        $communications = '[';
        foreach ($rq_communication as $key => $value) {
            $communications .= (($key>0) ? ',' : '').'{';
                $communications .= '"skill":"'.$value.'"';
                $communications .= ', "score":"'.$rq_comm_score[$key].'"';
            $communications .= '}';
        }
        $communications .= ']';

        $rq_interest = $request->get('interest');
        $interests = '[';
        foreach ($rq_interest as $key => $value) {
            $interests .= (($key>0) ? ',' : '').'{';
                $interests .= '"detail":"'.$value.'"';
            $interests .= '}';
        }
        $interests .= ']';

        $rq_position = $request->get('position');
        $rq_company = $request->get('company');
        $rq_from = $request->get('period_from');
        $rq_to = $request->get('period_to');
        $rq_position_detail = $request->get('position_detail');
        $employments = '[';
        foreach ($rq_position as $key => $value) {
            $employments .= (($key>0) ? ',' : '').'{';
                $employments .= '"position":"'.$value.'"';
                $employments .= ', "company":"'.$rq_company[$key].'"';
                $employments .= ', "from":"'.$rq_from[$key].'"';
                $employments .= ', "to":"'.$rq_to[$key].'"';
                $employments .= ', "detail":"'.preg_replace('/(\r\n)|\n|\r/', '\\n', $rq_position_detail[$key]).'"';
            $employments .= '}';
        }
        $employments .= ']';

        $rq_education = $request->get('education');
        $educations = '[';
        foreach ($rq_education as $key => $value) {
            $educations .= (($key>0) ? ',' : '').'{';
                $educations .= '"detail":"'.$value.'"';
            $educations .= '}';
        }
        $educations .= ']';

        $rq_computer = $request->get('computer');
        $computers = '[';
        foreach ($rq_computer as $key => $value) {
            $computers .= (($key>0) ? ',' : '').'{';
                $computers .= '"detail":"'.$value.'"';
            $computers .= '}';
        }
        $computers .= ']';

        if ($request->file('photo')->isValid()) {
            $path = $request->file('photo')->store('images');
        }
        
        $cv = new Personal;
        $cv->name = $request->name;
        $cv->image = $path;
        $cv->age = $request->age;
        $cv->telephone = $request->telephone;
        $cv->email = $request->email;
        $cv->address = $request->address;
        $cv->about_me = $request->about_me;
        $cv->skills = $skills;
        $cv->communications = $communications;
        $cv->interests = $interests;
        $cv->employments = $employments;
        $cv->educations = $educations;
        $cv->computers = $computers;
        $saved = $cv->save();
        $message = (!$saved) ? 'Save to database failed!!!' : 'Successful!';
        return redirect('/')->with('status', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Personal::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'age' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'about_me' => 'required',
            'skill.*' => 'required',
            'skill_score.*' => 'required',
            'skill_full_score.*' => 'required',
            'communication.*' => 'required',
            'comm_score.*' => 'required',
            'interest.*' => 'required',
            'position.*' => 'required',
            'company.*' => 'required',
            'period_from.*' => 'required',
            'period_to.*' => 'required',
            'position_detail.*' => 'required',
            'education.*' => 'required',
            'computer.*' => 'required',
        ]);
        
        $rq_skills = $request->get('skill');
        $rq_skill_score = $request->get('skill_score');
        $rq_skill_full_score = $request->get('skill_full_score');
        $skills = '[';
        foreach ($rq_skills as $key => $value) {
            $skills .= (($key>0) ? ',' : '').'{';
                $skills .= '"skill":"'.$value.'"';
                $skills .= ', "skill_score":"'.$rq_skill_score[$key].'"';
                $skills .= ', "skill_full_score":"'.$rq_skill_full_score[$key].'"';
            $skills .= '}';
        }
        $skills .= ']';

        $rq_communication = $request->get('communication');
        $rq_comm_score = $request->get('comm_score');
        $communications = '[';
        foreach ($rq_communication as $key => $value) {
            $communications .= (($key>0) ? ',' : '').'{';
                $communications .= '"skill":"'.$value.'"';
                $communications .= ', "score":"'.$rq_comm_score[$key].'"';
            $communications .= '}';
        }
        $communications .= ']';

        $rq_interest = $request->get('interest');
        $interests = '[';
        foreach ($rq_interest as $key => $value) {
            $interests .= (($key>0) ? ',' : '').'{';
                $interests .= '"detail":"'.$value.'"';
            $interests .= '}';
        }
        $interests .= ']';

        $rq_position = $request->get('position');
        $rq_company = $request->get('company');
        $rq_from = $request->get('period_from');
        $rq_to = $request->get('period_to');
        $rq_position_detail = $request->get('position_detail');
        $employments = '[';
        foreach ($rq_position as $key => $value) {
            $employments .= (($key>0) ? ',' : '').'{';
                $employments .= '"position":"'.$value.'"';
                $employments .= ', "company":"'.$rq_company[$key].'"';
                $employments .= ', "from":"'.$rq_from[$key].'"';
                $employments .= ', "to":"'.$rq_to[$key].'"';
                $employments .= ', "detail":"'.preg_replace('/(\r\n)|\n|\r/', '\\n', $rq_position_detail[$key]).'"';
            $employments .= '}';
        }
        $employments .= ']';

        $rq_education = $request->get('education');
        $educations = '[';
        foreach ($rq_education as $key => $value) {
            $educations .= (($key>0) ? ',' : '').'{';
                $educations .= '"detail":"'.$value.'"';
            $educations .= '}';
        }
        $educations .= ']';

        $rq_computer = $request->get('computer');
        $computers = '[';
        foreach ($rq_computer as $key => $value) {
            $computers .= (($key>0) ? ',' : '').'{';
                $computers .= '"detail":"'.$value.'"';
            $computers .= '}';
        }
        $computers .= ']';

        $cv = Personal::find($id);
        $path = $cv->image;
        if ($request->has('photo')) {
            Storage::delete($cv->image);
            if ($request->file('photo')->isValid()) {
                $path = $request->file('photo')->store('images');
            }
        }
        $cv->name = $request->name;
        $cv->image = $path;
        $cv->age = $request->age;
        $cv->telephone = $request->telephone;
        $cv->email = $request->email;
        $cv->address = $request->address;
        $cv->about_me = $request->about_me;
        $cv->skills = $skills;
        $cv->communications = $communications;
        $cv->interests = $interests;
        $cv->employments = $employments;
        $cv->educations = $educations;
        $cv->computers = $computers;
        $saved = $cv->save();
        $message = (!$saved) ? 'Save to database failed!!!' : 'Successful!';
        return redirect('/')->with('status', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cv = Personal::find($id);
        Storage::delete($cv->image);
        $deleted = $cv->delete();
        $message = (!$deleted) ? 'Delete failed!!!' : 'Deleted successfully!';
        return redirect('/')->with('status', $message);
    }
}
