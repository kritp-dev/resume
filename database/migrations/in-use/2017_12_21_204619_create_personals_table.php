<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('image', 255)->nullable();
            $table->tinyInteger('age')->default(20);
            $table->string('telephone', 255);
            $table->string('email', 255);
            $table->text('address')->nullable();
            $table->text('about_me')->nullable();
            $table->text('skills')->nullable();
            $table->text('communications')->nullable();
            $table->text('interests')->nullable();
            $table->text('employments')->nullable();
            $table->text('educations')->nullable();
            $table->text('computers')->nullable();
            $table->timestamp('date_updated')->nullable();
            $table->timestamp('date_created')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personals');
    }
}
