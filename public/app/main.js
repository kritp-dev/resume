var app = angular.module('autocv', ['ngRoute'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});
app.config(['$locationProvider', '$routeProvider',
  function config($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    
    $routeProvider
      .when('/', {
        templateUrl: 'templates/cv-list.html',
        controller: 'cvListController',
      })
      .when('/new', {
        templateUrl: 'templates/cv-form.html',
        controller: 'cvNewController',
      })
      .when('/:cvId', {
        templateUrl: 'templates/cv-detail.html',
        controller: 'cvShowController',
      })
      .when('/:cvId/edit', {
        templateUrl: 'templates/cv-form.html',
        controller: 'cvEditController',
      })
      .otherwise({ redirectTo: '/' });
  }
]);

app.controller('cvListController', function($scope, $http) {
  $http.get('/api/cv').then(function(response) {
    $scope.lists = response.data;
  });

  $scope.delete = function(id) {
    $http({
      method: 'DELETE',
      url: '/cv/'+id
    });
    location.href = '/';
  }
});
app.controller('cvShowController', function($scope, $http, $routeParams) {
  $http.get('/api/cv/'+$routeParams.cvId).then(function(response) {
    $scope.cv = response.data;
    $scope.skills = JSON.parse($scope.cv.skills);
    $scope.communications = JSON.parse($scope.cv.communications);
    $scope.interests = JSON.parse($scope.cv.interests);
    $scope.employments = JSON.parse($scope.cv.employments);
    $scope.educations = JSON.parse($scope.cv.educations);
    $scope.computers = JSON.parse($scope.cv.computers);

    $scope.range = function(min, max, step) {
      step = step || 1;
      let arr = [];
      for (var i=min; i<=max; i+=step)
        arr.push(i);
      
      return arr;
    }
  });
});
app.controller('cvNewController', function($scope) {
  let min = 1;
  let max = 5;
  let obj_skill = { skill:'', score:0, full_score:5 };
  let obj_communication = { skill:'', score:'Excellent' };
  let obj_interest = { detail:'' };
  let obj_employment = { position:'', company:'', from:'', to:'', detail:'' };
  let obj_education = { detail:'' };
  let obj_computer = { detail:'' };

  $scope.opt_action = '/cv';
  $scope.opt_method = 'POST';
  $scope.photo = 'img/avatar.jpg';
  $scope.fullname = '';
  $scope.age = '';
  $scope.telephone = '';
  $scope.email = '';
  $scope.address = '';
  $scope.about_me = '';
  $scope.skills = [obj_skill];
  $scope.skill_increment = function() {
    if ($scope.skills.length<max)
      $scope.skills.push(obj_skill);
  }
  $scope.skill_decrement = function(index) {
    if ($scope.skills.length>min)
      $scope.skills.splice(index, 1);
  }

  $scope.communications = [obj_communication];
  $scope.comm_increment = function() {
    if ($scope.communications.length<max)
      $scope.communications.push(obj_communication);
  }
  $scope.comm_decrement = function(index) {
    if ($scope.communications.length>min)
      $scope.communications.splice(index, 1);
  }

  $scope.interests = [obj_interest];
  $scope.int_increment = function() {
    if ($scope.interests.length<max)
      $scope.interests.push(obj_interest);
  }
  $scope.int_decrement = function(index) {
    if ($scope.interests.length>min)
      $scope.interests.splice(index, 1);
  }

  $scope.employments = [obj_employment];
  $scope.emp_increment = function() {
    if ($scope.employments.length<max)
      $scope.employments.push(obj_employment);
  }
  $scope.emp_decrement = function(index) {
    if ($scope.employments.length>min)
      $scope.employments.splice(index, 1);
  }

  $scope.educations = [obj_education];
  $scope.edu_increment = function() {
    if ($scope.educations.length<max)
      $scope.educations.push(obj_education);
  }
  $scope.edu_decrement = function(index) {
    if ($scope.educations.length>min)
      $scope.educations.splice(index, 1);
  }

  $scope.computers = [obj_computer];
  $scope.comp_increment = function() {
    if ($scope.computers.length<max)
      $scope.computers.push(obj_computer);
  }
  $scope.comp_decrement = function(index) {
    if ($scope.computers.length>min)
      $scope.computers.splice(index, 1);
  }
});
app.controller('cvEditController', function($scope, $http, $routeParams) {
  $http.get('/api/cv/'+$routeParams.cvId).then(function(response) {
    $scope.cv = response.data;
    
    let min = 1;
    let max = 5;
    let obj_skill = { skill:'', score:0, full_score:5 };
    let obj_communication = { skill:'', score:'Excellent' };
    let obj_interest = { detail:'' };
    let obj_employment = { position:'', company:'', from:'', to:'', detail:'' };
    let obj_education = { detail:'' };
    let obj_computer = { detail:'' };
    
    $scope.opt_action = '/cv/'+$routeParams.cvId;
    $scope.opt_method = 'PUT';
    $scope.photo = ($scope.cv.image) ? './storage/'+$scope.cv.image : 'img/avatar.jpg';
    $scope.fullname = $scope.cv.name;
    $scope.age = $scope.cv.age;
    $scope.telephone = $scope.cv.telephone;
    $scope.email = $scope.cv.email;
    $scope.address = $scope.cv.address;
    $scope.about_me = $scope.cv.about_me;
    $scope.skills = JSON.parse($scope.cv.skills);
    $scope.skill_increment = function() {
      if ($scope.skills.length<max)
        $scope.skills.push(obj_skill);
    }
    $scope.skill_decrement = function(index) {
      if ($scope.skills.length>min)
        $scope.skills.splice(index, 1);
    }
    
    $scope.communications = JSON.parse($scope.cv.communications);
    $scope.comm_increment = function() {
      if ($scope.communications.length<max)
        $scope.communications.push(obj_communication);
    }
    $scope.comm_decrement = function(index) {
      if ($scope.communications.length>min)
        $scope.communications.splice(index, 1);
    }
  
    $scope.interests = JSON.parse($scope.cv.interests);
    $scope.int_increment = function() {
      if ($scope.interests.length<max)
        $scope.interests.push(obj_interest);
    }
    $scope.int_decrement = function(index) {
      if ($scope.interests.length>min)
        $scope.interests.splice(index, 1);
    }
  
    $scope.employments = JSON.parse($scope.cv.employments);
    $scope.emp_increment = function() {
      if ($scope.employments.length<max)
        $scope.employments.push(obj_employment);
    }
    $scope.emp_decrement = function(index) {
      if ($scope.employments.length>min)
        $scope.employments.splice(index, 1);
    }
  
    $scope.educations = JSON.parse($scope.cv.educations);
    $scope.edu_increment = function() {
      if ($scope.educations.length<max)
        $scope.educations.push(obj_education);
    }
    $scope.edu_decrement = function(index) {
      if ($scope.educations.length>min)
        $scope.educations.splice(index, 1);
    }
  
    $scope.computers = JSON.parse($scope.cv.computers);
    $scope.comp_increment = function() {
      if ($scope.computers.length<max)
        $scope.computers.push(obj_computer);
    }
    $scope.comp_decrement = function(index) {
      if ($scope.computers.length>min)
        $scope.computers.splice(index, 1);
    }
  });
});
