var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
  browserSync({
      server: {
          baseDir: "./"
      },
      port: 8000,
  });
});

gulp.task('default', ['browser-sync'], function() {
  gulp.watch(['./public/css/*.css'], browserSync.reload);
  gulp.watch(['./public/js/*.js'], browserSync.reload);
  gulp.watch(['./public/templates/*.html'], browserSync.reload);
});